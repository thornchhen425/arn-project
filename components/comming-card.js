const apiResult = [
    {
      image: "book.jpeg",
      title: "title1",
      description:
        "It's an alphabet book that aims to help us share what we do with the kids in our life, and spark children's interest in coding.",
      price: 20,
    },
    {
      image: "book.jpeg",
      title: "title1",
      description:
        "It's an alphabet book that aims to help us share what we do with the kids in our life, and spark children's interest in coding.",
      price: 20,
    },
    {
      image: "book.jpeg",
      title: "title1",
      description:
        "It's an alphabet book that aims to help us share what we do with the kids in our life, and spark children's interest in coding.",
      price: 20,
    },
    {
      image: "book.jpeg",
      title: "title1",
      description:
        "It's an alphabet book that aims to help us share what we do with the kids in our life, and spark children's interest in coding.",
      price: 20,
    },
    {
      image: "book.jpeg",
      title: "title1",
      description:
        "It's an alphabet book that aims to help us share what we do with the kids in our life, and spark children's interest in coding.",
      price: 20,
    },
    {
      image: "book.jpeg",
      title: "title1",
      description:
        "It's an alphabet book that aims to help us share what we do with the kids in our life, and spark children's interest in coding.",
      price: 20,
    },
    {
      image: "book.jpeg",
      title: "title1",
      description:
        "It's an alphabet book that aims to help us share what we do with the kids in our life, and spark children's interest in coding.",
      price: 20,
    },
    {
      image: "book.jpeg",
      title: "title1",
      description:
        "It's an alphabet book that aims to help us share what we do with the kids in our life, and spark children's interest in coding.",
      price: 20,
    },
  ];

  const container = document.getElementById("comming-card");

  apiResult.forEach((result, idx) => {
    // Create card element
    const card = document.createElement("div");
    card.classList = "card-body";

    const content = `
    <div class="row">
    <div class="col-lg-3">
    <div class="card">
    <img class="img-card" src=${result.image}>
    <div class="card-body">
    <h5 class="card-title">${result.title}</h5>
    <p class="card-text">${result.description}</p>
    <h3 style="font-size: 20px;">តម្លៃ ${result.price}$</3>
    <div style="float: right;">
        <a href="#" class="btn btn-primary">មើល</a>
        <a href="#" class="btn btn-danger">បញ្ជាទិញ</a>
    </div>
    </div>
    </div>
    </div>
    </div>
`;

    // Append newyly created card element to the container
    container.innerHTML += content;
  });